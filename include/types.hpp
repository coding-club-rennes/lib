/*
** types.hpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub/context
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 14:05:11 2015 Alexis Bertholom
** Last update Tue Jan 27 14:05:12 2015 Alexis Bertholom
*/

#ifndef TYPES_H_
# define TYPES_H_

typedef int		GLint;
typedef unsigned int	GLuint;
typedef unsigned int	GLenum;
typedef unsigned int	Uint32;
typedef unsigned int	Uint;
typedef unsigned char	Uchar;

typedef Uint	Color;

#endif
